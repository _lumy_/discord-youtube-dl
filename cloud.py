import owncloud
import os, json

USER_DIR = "users"
ADMIN_DIR = "admin"

def create_oc(fn):
  def func(*args, **kwargs):
    with open("cloud.json") as f:
      config = json.load(f)
    oc = owncloud.Client(config['DOMAIN'])
    oc.login(config['LOGIN'], config['PASSWORD'])
    ret = fn(oc, *args, **kwargs)
    oc.logout()
    return ret
  return func

def make_dir(oc, directory):
  """
    Make a dir on remote server.
  Catch if folder already exists.
  """
  try:
    oc.mkdir(directory)
  except owncloud.owncloud.HTTPResponseError:
    pass

@create_oc
def upload_and_share(oc, filepath, admin=False):
  make_dir(oc, USER_DIR)
  make_dir(oc, ADMIN_DIR)
  remote_path = "%s/%s" % (USER_DIR, filepath)
  if admin:
    remote_path = "%s/%s" % (ADMIN_DIR, filepath)
  try:
    FileInfo = oc.file_info(remote_path)
    ShareInfo = oc.get_shares(remote_path)[0]
    return ShareInfo.get_link()
  except owncloud.owncloud.HTTPResponseError:
    oc.put_file(remote_path, filepath)
    ShareInfo = oc.share_file_with_link(remote_path, name=filepath)
  except IndexError:
    ShareInfo = oc.share_file_with_link(remote_path, name=filepath)
  return ShareInfo.get_link()
