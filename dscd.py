#!/usr/bin/env python3

import os
import discord
import cloud
import logging
import re
from ytb import download_info, download, clean_subtitle

regex = """.*(https://(www.)?youtu(.be|be.com)/(.*?)) +.*"""
compiled_regex = re.compile(regex)
client = discord.Client()
help_message = """
Hello I'm the youtube bot:
Usage is:
?ytb help # Show this help message
?ytb info youtube_link # Show different stats about a youtube video
?ytb subtitle youtube_link #
?ytb subtitle_only youtube_link #
?ytb download youtube_link #
"""
async def subtitle_and_repost(message, args):
  await download_and_repost(message, args, remove=False)
  await subtitle_only(message, args, remove=True)

async def subtitle_only(message, args, remove=True):
  info_dict = clean_subtitle(args[0])
  response = """I've extracted subtitle from %s.""" % info_dict['title']
  await message.channel.send(content=response,
                             file=discord.File("%s_script.txt" % info_dict['title']))
  if remove:
    os.remove("%s.%s" % (info_dict['title'], info_dict['ext']))
    os.remove("%s.fr.vtt" % info_dict['title'])
    os.remove("%s_script.txt" % info_dict['title'])

async def download_and_repost(message, args, admin=True, remove=True):
  info = download(args[0], subtitle=False)
  # Depending of size we should repost or not.
  response = """Une vidéo YouTube ({title}) a été posté et a été vu {views} fois sur Youtube.
  Si vous voulez la voir sans augmenter sa visibilité, vous pouvez la visionner directement via ce message.""".format(title=info['title'], views=info['view_count'])
  try:
    print("Trying to post message")
    await message.channel.send(content=response, file=discord.File("%s.%s" % (info['title'], info['ext'])))
  except discord.errors.HTTPException as e:
    link_location = cloud.upload_and_share("%s.%s" % (info['title'], info['ext']), admin=admin)
    response = """Une vidéo YouTube ({title}) a été posté et a été vu {views} fois sur Youtube.
Si vous voulez la voir sans augmenter sa visibilité, elle a ete uploader sur owncloud.
Voici le lien:{link}""".format(title=info['title'], views=info['view_count'], link=link_location)
    await message.channel.send(content=response)
  os.remove("%s.%s" % (info['title'], info['ext']))

async def get_stats(message, args):
  """
  Maybe the description of the video is too much.
  """
  info_dict = download_info(args[0])
  response = """The video %s has been viewed %s times\nStats are:\n
%s dislike\n%s like\n%s average_rating\nDescription: %s""" % (info_dict['title'],
                                             info_dict['view_count'],
                                             info_dict['dislike_count'],
                                             info_dict['like_count'],
                                             info_dict['average_rating'],
                                             info_dict['description'])
  await message.channel.send(content=response)

async def show_help(message, args):
  await message.channel.send(content=help_message)

async def user_download(message, args):
  pass

commands = {
  "download": download_and_repost,
  "subtitle_only": subtitle_only,
  "subtitle": subtitle_and_repost,
  "info": get_stats,
  "help": show_help,
}

service_call = "?ytb "

async def manage_command(message):
  list_commands = message.content.strip().split(" ")
  await commands[list_commands[1]](message, list_commands[2:])

@client.event
async def on_message(message):
  if message.author == client.user:
    return
  try:
    if any(x.name == "Modérateur" for x in message.author.roles) and message.channel.name == "modération":
      if message.content.startswith(service_call):
        await manage_command(message)
        return

  except AttributeError as e:
    print(e)

  # Redo this part to match all youtube link and do a default download and repost
#   result = compiled_regex.match(message.content)
#   try:
#     youtube_link = result.group(1)
#     title, ext, views = youtube_download(youtube_link)
#     response = """Une vidéo YouTube ({title}) a été posté et a été vu {views} fois sur Youtube.
# Si vous voulez la voir sans augmenter sa visibilité, vous pouvez la visionner directement via ce message.""".format(title=title, views=views)
#     await message.channel.send(content=response, file=discord.File("%s.%s" % (title, ext)))
#     os.remove("%s.%s" % (title, ext))
#   except AttributeError:
#     pass

try:
  with open('token') as f:
    print("Starting Bot Discord")
    client.run(f.read())
except FileNotFoundError:
  print("No token file")
